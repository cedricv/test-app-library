# Sentinel C demo app library

This is a demo SentinelC app library.

Fork it and use it as a starting point for packaging your own app library.

See [app-library-builder](https://gitlab.com/sentinelc/app-library-builder) for full details and specs.

