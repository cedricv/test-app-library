# TShark

This recipe will run TShark on a dedicated Ethernet port. You will see all packets received get printed in the logs.

The recipe requires an available Ethernet port on the appliance where it is installed. The port will be dedicated to the
service and cannot be used for anything else once the service is installed.

To free the port, uninstall the service.

