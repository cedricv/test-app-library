# Test degraded
This image contains 1 container that is set to crash after a predetermined number of seconds and
another container that is set to be running. The pod should become in degraded state.
This is for testing purposes only.
