## Firefox in a container

This is a SentinelC package of https://github.com/jlesage/docker-firefox

Firefox is launched in a container and accessed from a modern web browser.

---

Mozilla Firefox is a free and open-source web browser developed by Mozilla
Foundation and its subsidiary, Mozilla Corporation.
