# Demo-Form

Pour démonstation/test uniquement.

Lors de l'installation de ce service, un formulaire de démonstration affichera plusieurs paramètres afin de démontrer
les types de champs supportés et leurs options.

L'application lancée est Echo Server, ce qui est utile pour voir les variables reçues du formulaire d'installation.

Plus de documentation ici: https://github.com/Ealenn/Echo-Server
