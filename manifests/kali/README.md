# Kali Linux adapted for SentinelC

Kali linux accessible using a web-based terminal.

This is a minimal kali install, the following packages are pre-installed:

- whois
- nano
- curl
- wget
- iputils-ping
- python3
- iproute2
- command-not-found
- bind9-dnsutils

Install more tools using:

```
apt update
apt install <package>
```
